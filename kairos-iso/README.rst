Kairos ISO
==========

This bootstrap Terraform component is responsible for creating a bootable ISO
for installing and configuring a first Kubernetes node.

.. mermaid::

  flowchart TB

    operator{{Human Operator}}
    subgraph "Local Workstation"
      terragrunt(terragrunt apply)
      subgraph podman
        podman_system_service("podman system service")
        podman_sock(["podman.sock"])
        podman_system_service <--> podman_sock
      end
      osbuilder_tools("osbuilder_tools\n(podman run ... osbuilder-tools:latest)")
      cloud_config("cloud_config.yaml")
      iso("elemental.iso")
    end
    subgraph "Public Container Registry"
      kairos_base_image("kairos-opensuse")
      kairos_container_image("bauernet-kairos-container")
      kairos_base_image --> kairos_container_image
    end

    operator -- Run Process --> podman_system_service & terragrunt
    terragrunt -- Build Cloud Config --> cloud_config
    terragrunt -- Invoke --> osbuilder_tools
    podman_sock & cloud_config -. Bind Mount .-> osbuilder_tools
    kairos_container_image -. Pull Container .-> osbuilder_tools
    osbuilder_tools -- Assemble ISO --> iso

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :language: terraform
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: main.tf
  :language: terraform
  :caption: ``main.tf``
