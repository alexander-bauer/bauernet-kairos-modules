variable "bootstrap_hostname" {
  type = string
}
variable "bootstrap_install_device" {
  type = string
}
variable "kairos_image" {
  type    = string
  default = "registry.gitlab.com/alexander-bauer/bauernet-kairos-container:main"
}
variable "ssh_authorized_keys" {
  type = list(string)
}
variable "podman_sock" {
  type = string
}
variable "pull_base_container" {
  type    = bool
  default = true
}
variable "kube_vip" {
  type = string
}
variable "kube_vip_interface" {
  type = string
}
variable "service_vip" {
  type = string
}

locals {
  out_base     = "${path.module}/out"
  overlay_dir  = "overlay"
  overlay_base = "${local.out_base}/${local.overlay_dir}"
}

# Pull the OSBuilder Tools image.
resource "docker_image" "osbuilder_tools" {
  name = "quay.io/kairos/osbuilder-tools:latest"
}

# We either query the specified local image (so that we can use its digest to
# specify), or we look up the digest from the remote.
data "docker_image" "kairos_image" {
  count = var.pull_base_container ? 0 : 1
  name  = var.kairos_image
}
data "docker_registry_image" "kairos_image" {
  count = 1 - length(data.docker_image.kairos_image)
  name  = var.kairos_image
}

locals {
  # Assemble the complete image name with digest. We have different variables
  # to work with depending on whether we're using docker_image or
  # docker_registry_image.
  #
  # ``docker_image``
  #
  #   ::
  #
  #     # data.docker_image.kairos_image[0]:
  #     data "docker_image" "kairos_image" {
  #         id          = "sha256:2cefde772f060742d116029b42ef917ee03117fc5715e81aaf78a1880b4927aa"
  #         name        = "registry.gitlab.com/alexander-bauer/bauernet-kairos-container:main"
  #         repo_digest = "registry.gitlab.com/alexander-bauer/bauernet-kairos-container@sha256:d0bf375b8a34bc61d48797439894a759fca7d77da6851d4acf8650881af602d0"
  #     }
  #
  # ``docker_registry_image``
  #
  #   ::
  #
  #     # data.docker_registry_image.kairos_image[0]:
  #     data "docker_registry_image" "kairos_image" {
  #         id                   = "sha256:6c1ada1e4f8dc3598d72e766599e4651e8237cae7519125938c2a75eb2947d49"
  #         insecure_skip_verify = false
  #         name                 = "registry.gitlab.com/alexander-bauer/bauernet-kairos-container:main"
  #         sha256_digest        = "sha256:6c1ada1e4f8dc3598d72e766599e4651e8237cae7519125938c2a75eb2947d49"
  #     }
  #
  # So in order to assemble the correct string (<registry><:tag, optionally>@<digest>), we do the following.
  kairos_image_digest = var.pull_base_container ? "${var.kairos_image}@${one(data.docker_registry_image.kairos_image).sha256_digest}" : one(data.docker_image.kairos_image).repo_digest
}

module "kairos_config" {
  source = "../kairos-config"

  hostname       = var.bootstrap_hostname
  control_plane  = true
  install_device = var.bootstrap_install_device

  ssh_authorized_keys = var.ssh_authorized_keys

  k3s_env = {
    # The first node has to have cluster init specified.
    K3S_CLUSTER_INIT = "true"
  }

  kube_vip           = var.kube_vip
  kube_vip_interface = var.kube_vip_interface
  service_vip        = var.service_vip
}

# Place the config.yaml
resource "local_file" "config" {
  filename        = "${local.overlay_base}/cloud_config.yaml"
  file_permission = "0644"
  content         = module.kairos_config.cloud_config
}

# Place extra files for manual inspection
resource "local_file" "extra_files" {
  for_each        = { for d in module.kairos_config.extra_files : basename(d.path) => d }
  filename        = "${local.out_base}/${each.key}"
  file_permission = "0600"
  content         = each.value.content
}

# Run the container
resource "null_resource" "iso_create" {
  triggers = {
    "osbuilder_tools_image" = docker_image.osbuilder_tools.image_id
    "kairos_image"          = local.kairos_image_digest
    "config.yaml"           = local_file.config.id
  }

  provisioner "local-exec" {
    command = join(" ", [
      "podman", "run", "--rm",
      "-v", "${local.out_base}:/cOS",
      "-v", "${var.podman_sock}:/var/run/docker.sock",
      docker_image.osbuilder_tools.name,
      "--debug", "build-iso",
      "--date=false",
      "--local",
      "--overlay-iso", "/cOS/${local.overlay_dir}",
      # Last arguments are <image> and --output and <output volume mount>
      local.kairos_image_digest, "--output", "/cOS/"
    ])
  }
}

output "iso_path" {
  value = "${local.out_base}/elemental.iso"
}
