#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.3.0] - 2023-08-20
====================

Added
-----

* New variables ``osbuilder_chart_version`` and ``crds_chart_version`` to
  ``kairos-k8s``.

Changed
-------

* Update to using ``v1alpha2`` version Kairos CRDs.


[0.2.0] - 2023-05-01
====================

Added
-----

* ``kairos-iso`` and ``kairos-k8s`` modules from original ``bauernet`` repository

[0.1.0] - 2023-05-01
====================

Added
-----

* Initial content migrated from original ``bauernet`` repository
