variable "kube_vip_ip" {
  type = string
}
variable "kube_vip_interface" {
  type = string
}
variable "kube_vip_image" {
  type    = string
  default = "ghcr.io/kube-vip/kube-vip:v0.5.7"
}
variable "deploy_ccm" {
  type = bool
}

locals {
  kube_vip_daemonset = yamlencode({
    apiVersion = "apps/v1"
    kind       = "DaemonSet"
    metadata = {
      creationTimestamp = null
      name              = "kube-vip"
      namespace         = "kube-system"
    }
    spec = {
      selector = {
        matchLabels = {
          name = "kube-vip"
        }
      }
      template = {
        metadata = {
          creationTimestamp = null
          labels = {
            name = "kube-vip"
          }
        }
        spec = {
          affinity = {
            nodeAffinity = {
              requiredDuringSchedulingIgnoredDuringExecution = {
                nodeSelectorTerms = [
                  {
                    matchExpressions = [{
                      key      = "node-role.kubernetes.io/master"
                      operator = "Exists"
                    }]
                  },
                  {
                    matchExpressions = [{
                      key      = "node-role.kubernetes.io/control-plane"
                      operator = "Exists"
                    }]
                  }
                ]
              }
            }
          }
          containers = [{
            args = ["manager"]
            env = [
              { name = "vip_arp", value = "true" },
              { name = "port", value = "6443" },
              { name = "vip_interface", value = var.kube_vip_interface },
              { name = "vip_cidr", value = "32" },
              { name = "cp_enable", value = "true" },
              { name = "cp_namespace", value = "kube-system" },
              { name = "vip_ddns", value = "false" },
              { name = "svc_enable", value = "true" },
              { name = "vip_leaderelection", value = "true" },
              { name = "vip_leaseduration", value = "5" },
              { name = "vip_renewdeadline", value = "3" },
              { name = "vip_retryperiod", value = "1" },
              { name = "address", value = var.kube_vip_ip },
            ]
            image           = var.kube_vip_image
            imagePullPolicy = "IfNotPresent"
            name            = "kube-vip"
            resources       = {}
            securityContext = {
              capabilities = {
                add = ["NET_ADMIN", "NET_RAW", "SYS_TIME"]
              }
            }
          }]
          hostNetwork        = true
          serviceAccountName = "kube-vip"
          tolerations = [
            { effect = "NoSchedule", operator = "Exists" },
            { effect = "NoExecute", operator = "Exists" },
          ]
        }
      }
    }
    status = {
      currentNumberScheduled = 0
      desiredNumberScheduled = 0
      numberMisscheduled     = 0
      numberReady            = 0
    }
  })
  kube_vip_ccm_configmap = yamlencode({
    apiVersion = "v1"
    kind       = "ConfigMap"
    metadata = {
      name      = "kubevip"
      namespace = "kube-system"
    }
    data = {
      "cidr-global" = "${var.kube_vip_ip}/32"
    }
  })
}

data "http" "kube_vip_rbac" {
  url = "https://kube-vip.io/manifests/rbac.yaml"
}

data "http" "kube_vip_ccm" {
  count = var.deploy_ccm ? 1 : 0
  url   = "https://raw.githubusercontent.com/kube-vip/kube-vip-cloud-provider/main/manifest/kube-vip-cloud-controller.yaml"
}

output "manifests" {
  value = compact([
    data.http.kube_vip_rbac.response_body,
    var.deploy_ccm ? one(data.http.kube_vip_ccm).response_body : "",
    local.kube_vip_daemonset,
    var.deploy_ccm ? local.kube_vip_ccm_configmap : "",
  ])
}
