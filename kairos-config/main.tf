variable "hostname" {
  type = string
}
variable "install_device" {
  type = string
}
variable "ssh_authorized_keys" {
  type = list(string)
}
variable "control_plane" {
  type = bool
}
variable "k3s_env" {
  type    = map(string)
  default = {}
}
variable "kube_vip" {
  type = string
}
variable "kube_vip_interface" {
  type = string
}
variable "service_vip" {
  type = string
}
variable "use_service_vip_for_resolv_conf" {
  type        = bool
  default     = true
  description = "Configure an alterative resolv.conf for kubelet that points to the service_vip"
}

module "kubevip_manifests" {
  source = "../kairos-kubevip-manifests"

  kube_vip_ip        = var.kube_vip
  kube_vip_interface = var.kube_vip_interface
  deploy_ccm         = false # metallb provides this function
}

module "metallb_manifests" {
  source = "../kairos-metallb-manifests"

  vip = var.service_vip
}

locals {
  kubevip_files = [{
    path        = "/var/lib/rancher/k3s/server/manifests/kubevip.yaml"
    owner       = "root"
    permissions = "0644"
    content     = join("\n---\n", module.kubevip_manifests.manifests)
  }]
  metallb_files = [{
    path        = "/var/lib/rancher/k3s/server/manifests/metallb.yaml"
    owner       = "root"
    permissions = "0644"
    content     = join("\n---\n", module.metallb_manifests.manifests)
  }]
  kubelet_resolvconf = var.use_service_vip_for_resolv_conf ? [{
    path        = "/var/lib/rancher/k3s/server/kubelet-resolv.conf"
    owner       = "root"
    permissions = "0644"
    content     = <<-EOF
      # Kubelet resolv.conf pointed to Kubernetes-hosted DNS resolver
      nameserver ${var.service_vip}
    EOF
  }] : []

  extra_files = concat(local.kubevip_files, local.metallb_files, local.kubelet_resolvconf)

  k3s_server_args = [
      "--tls-san ${var.kube_vip}",
      "--flannel-backend=wireguard-native",
      "--disable local-storage",
      "--disable servicelb",
      "--disable traefik"
  ]

  k3s_agent_args = []

  k3s_common_args = var.use_service_vip_for_resolv_conf ? ["--resolv-conf ${one(local.kubelet_resolvconf).path}"] : []

  k3s_obj = {
    enabled = true
    env     = var.k3s_env
    args = concat(local.k3s_common_args, var.control_plane ? local.k3s_server_args : local.k3s_agent_args)

#    args = concat([
#      ],
#      var.use_service_vip_for_resolv_conf ? ["--resolv-conf ${one(local.kubelet_resolvconf).path}"] : []
#    )
  }

  cloud_config_obj = {
    hostname = var.hostname
    users = [{
      name                = "kairos"
      passwd              = "kairos"
      ssh_authorized_keys = var.ssh_authorized_keys
    }]
    #stages = {
    #  initramfs = [{
    #    name = "Wipe target disk (kairos-io/kairos#499)"
    #    commands = [
    #      "dd if=/dev/zero of=${var.install_device} bs=512 count=512"
    #    ]
    #  }]
    #}
    install = {
      device   = var.install_device
      reboot   = false
      poweroff = true
      auto     = true
    }
    # If the node is a control plane node, it gets "k3s" config, else "k3s-agent."
    k3s         = var.control_plane ? local.k3s_obj : null
    k3s-agent   = var.control_plane ? null : local.k3s_obj
    write_files = local.extra_files
  }
  cloud_config = <<-EOF
    #cloud-config
    ${yamlencode(local.cloud_config_obj)}
  EOF
}

output "cloud_config_object" {
  value = local.cloud_config_obj
}

output "cloud_config" {
  value = local.cloud_config
}

output "extra_files" {
  value = local.extra_files
}
