Kairos Config Module
====================

This module generates a cloud config for consumption by a Kairos installer.

DNS Resolution
--------------

This module contains an argument ``use_service_vip_for_resolv_conf`` which
defaults to ``true``. This controls whether K3s is configured to use an
alternative ``resolv.conf`` (as opposed to the host ``/etc/resolv.conf``.) The
alternative ``k3s-resolv.conf`` is generated to use the ``service_vip``, with
the expectation that once the cluster is living, an externally-resolving DNS
service will be listening at that address.

Terraform
---------

.. literalinclude:: main.tf
  :caption: ``main.tf``
