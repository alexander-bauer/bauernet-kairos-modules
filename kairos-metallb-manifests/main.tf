variable "vip" {
  type = string
}
variable "namespace" {
  type    = string
  default = "metallb-system"
}

## Use the helm_template data source to locally render all of the helm chart
## templates, so that they can be deployed instantly on k3s startup.
#data "helm_template" "metallb" {
#  name       = "metallb"
#  repository = var.metallb_helm_repository
#  chart      = var.metallb_helm_chart
#  version    = var.metallb_helm_chart_version
#
#  # Deploy into a custom namespace rather than default metallb-system.
#  namespace = var.namespace
#
#  #set {
#  #  name = "crds.validationFailurePolicy"
#  #  value = "Ignore"
#  #}
#}

data "http" "metallb" {
  url = "https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml"
}

locals {
  objects = [
    {
      apiVersion = "metallb.io/v1beta1"
      kind       = "IPAddressPool"
      metadata = {
        name      = "service-vip"
        namespace = var.namespace
      }
      spec = {
        addresses = ["${var.vip}/32"]
      }
    },
    {
      apiVersion = "metallb.io/v1beta1"
      kind       = "L2Advertisement"
      metadata = {
        name      = "arp-all"
        namespace = var.namespace
      }
      spec = {} # no spec needed - broadcast all IPs
    },
  ]
  manifests = [for o in local.objects : yamlencode(o)]
}

output "manifests" {
  value = concat([data.http.metallb.response_body], local.manifests)
}
