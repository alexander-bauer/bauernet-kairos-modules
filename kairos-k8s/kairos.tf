variable "devices" {
  type = map(object({
    mac           = string
    install_disk  = string
    control_plane = bool
    iso           = optional(bool, false)
    netboot       = optional(bool, false)
  }))
}
variable "kairos_image" {
  type    = string
  default = "registry.gitlab.com/alexander-bauer/bauernet-kairos-container:main"
}
variable "ssh_authorized_keys" {
  type = list(string)
}
variable "kube_vip" {
  type = string
}
variable "kube_vip_interface" {
  type = string
}
variable "service_vip" {
  type = string
}
variable "k3s_token" {
  type      = string
  sensitive = true
}

resource "kubernetes_namespace" "kairos" {
  metadata {
    name = "kairos"
  }
}

data "docker_registry_image" "kairos_image" {
  name = var.kairos_image
}

module "kairos_config" {
  source   = "../kairos-config"
  for_each = var.devices

  hostname       = each.key
  control_plane  = each.value.control_plane
  install_device = each.value.install_disk

  ssh_authorized_keys = var.ssh_authorized_keys

  k3s_env = {
    # Join using the vip.
    K3S_URL = "https://${var.kube_vip}:6443"
    # Use the token discovered from the secret.
    K3S_TOKEN = var.k3s_token
  }

  # kube-vip options
  kube_vip           = var.kube_vip
  kube_vip_interface = var.kube_vip_interface
  service_vip        = var.service_vip
}

#resource "kubectl_manifest" "kairos_bauernet_netboot" {
#  for_each = { for k, v in var.devices : k => v if v.netboot }
#  yaml_body = yamlencode({
#    apiVersion = "build.kairos.io/v1alpha2"
#    kind       = "OSArtifact"
#    metadata = {
#      name      = "bauernet-${each.key}"
#      namespace = one(kubernetes_namespace.kairos.metadata).name
#    }
#    spec = {
#      imageName   = "${data.docker_registry_image.kairos_image.name}@${dat#a.docker_registry_image.kairos_image.sha256_digest}"
#      iso         = false
#      netboot     = true
#      cloudConfig = module.kairos_config[each.key].cloud_config
#    }
#  })
#  # OSBuilder does not yet detect changes, so we must delete and recreate.
#  force_new = true
#}

resource "kubectl_manifest" "kairos_bauernet_iso" {
  depends_on = [helm_release.kairos_crds]
  for_each   = { for k, v in var.devices : k => v if v.iso }
  yaml_body = yamlencode({
    apiVersion = "build.kairos.io/v1alpha2"
    kind       = "OSArtifact"
    metadata = {
      name      = "bauernet-${each.key}"
      namespace = one(kubernetes_namespace.kairos.metadata).name
    }
    spec = {
      imageName = "${data.docker_registry_image.kairos_image.name}@${data.docker_registry_image.kairos_image.sha256_digest}"
      iso       = true
      netboot   = false
      #      cloudConfig = module.kairos_config[each.key].cloud_config
      exporters = [{
        template = {
          spec = {
            restartPolicy = "Never"
            containers = [{
              name = "upload"
              #            image = "docker.spectro.jbpe.io/kairos/osbuilder-tools:v0.7.0"
              #image   = "quay.io/kairos/osbuilder-tools:latest"
              image   = "quay.io/curl/curl:latest" # used only for upload
              command = ["sh"]
              args = [
                "-c",
                <<-EOF
                for f in $(ls /artifacts); do
                  curl --progress-bar -T "/artifacts/$f" "http://osartifactbuilder-operator-osbuilder-nginx/upload/$f"
                done
                EOF
              ]
              volumeMounts = [{
                name      = "artifacts"
                mountPath = "/artifacts"
              }] #volumemounts
            }]   #containers
          }      #spec
        }
      }] #exporters
      cloudConfigRef = {
        name = one(kubernetes_secret.cloud_config[each.key].metadata).name
        key  = one(keys(kubernetes_secret.cloud_config[each.key].data))
      }
    } #spec

  })
  # OSBuilder does not yet detect changes, so we must delete and recreate.
  force_new = true
}

resource "kubernetes_secret" "cloud_config" {
  for_each = module.kairos_config
  metadata {
    name      = "${each.key}-cloud-config"
    namespace = one(kubernetes_namespace.kairos.metadata).name
  }
  data = {
    userData = each.value.cloud_config
  }
}

#output "mycloud" {
#  value = kubernetes_secret.cloud_config["node5"].data
#  sensitive = true
#}

