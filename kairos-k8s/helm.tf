resource "helm_release" "kairos_crds" {
  name       = "kairos-crds"
  repository = "https://kairos-io.github.io/helm-charts"
  chart      = "kairos-crds"
  namespace  = one(kubernetes_namespace.kairos.metadata).name
  version    = var.crds_chart_version
}

resource "helm_release" "osbuilder" {
  name       = "osbuilder"
  repository = "https://kairos-io.github.io/helm-charts"
  chart      = "osbuilder"
  namespace  = one(kubernetes_namespace.kairos.metadata).name
  version    = var.osbuilder_chart_version
}
