variable "osbuilder_chart_version" {
  description = "Version of the Helm chart to install"
  type        = string
  default     = "0.6.0"
}

variable "crds_chart_version" {
  description = "Version of the Helm chart to install"
  type        = string
  default     = "0.0.14"
}
