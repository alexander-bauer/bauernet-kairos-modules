#!/bin/bash

set -ex

terraform apply -auto-approve
kubectl wait --for condition=Available=True --timeout -1s -n kairos deployment/bauernet-api-iso

kubectl delete job -n kairos image-bauernet-api
kubectl apply -f flash-job.yaml

kubectl wait --for condition=complete --timeout -1s -n kairos job/image-bauernet-api

notify-send "SD Card Image Job Complete"
