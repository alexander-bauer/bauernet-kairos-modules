Kairos
======

This component configures `Kairos OSBuilder
<https://kairos.io/docs/advanced/build/>`_ to build provisioning artifacts for
expanding the cluster. The configuration is generated using the
:doc:`same underlying module </terraform/modules/kairos-config/README>` that is
used by the :doc:`/terraform/components/bootstrap/kairos-iso/README`.

This component reads ``device`` configuration in order to generate one
``OSArtifact`` per device, either in ISO or in netboot mode. The ``OSArtifact``
custom resource is used by the OSBuilder controller to produce artifacts for
installing a particular node, with all of the customization it needs to join
the cluster and service workloads.

``OSArtifact`` resources ultimately render ``Deployments`` and connected
``Services`` which, once ready, serve the artifacts necessary for installation.
Part of the ``OSArtifact`` specification describes whether an ISO, nebooting
artifacts, or other files, are rendered. See below for how to consume these.

Consuming OSArtifacts
---------------------

Netbooting
^^^^^^^^^^

Netbooting is not yet implemented.


ISO
^^^

An ISO is a disk image; in this case, it is a bootable Kairos install medium
containing complete configuration for a particular node, which will
automatically install a customized Kairos OS to permanent boot media.

In order to use such an ISO, it must be flashed to removable media (like a
flash drive or microSD card), then inserted in the target device, and booted
from. Once the installation is complete, the machine will power off. Then, the
media can be removed, and the device powered back on, at which point it will
attempt to join the cluster.

Once a ``Deployment`` corresponding to a particular ``OSArtifact`` is ready,
the below ``flash-media`` ``Job`` can be applied to the cluster in order to
prepare installation media with a particular ISO. Using the Kubernetes cluster
for this eliminates the need for a sane external device to download the
artifact and flash it.

Physical devices, of course, are not fungible in the same way that compute
power is, so the ``Job`` leverages a unique node selector, in order to be sure
the correct medium is flashed. A particular node must be **manually** labeled
to perform this role. To do so, use the following code snippet, substituting
``<NODE>`` for the hostname of your node.

.. code-block:: console

   $ kubectl label node <NODE> node-role.kubernetes.io/media-provisioner=media-provisioner

Then, to flash media, apply this ``Job`` resource using the following snippet.
If you have created the ``Job`` already, you may need to first run ``kubectl
delete -n kairos job/flash-media``.

.. code-block:: console

   $ kubectl apply -f flash-media.yaml

.. warning::

   In the following code snippet, be careful to check the ``DEVICE`` and
   ``OSARTIFACT`` substitutions.

.. literalinclude:: flash-media.yaml
   :language: yaml
   :caption: ``flash-media.yaml``

Once applied, you can watch the logs using:

.. code-block:: console

   $ kubectl -n kairos logs job/flash-media
