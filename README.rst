#######################
BauerNet Kairos Modules
#######################

This repository contains modules related to the use of Kairos in the BauerNet
infrastructure.

.. toctree::
   :maxdepth: 1
   :glob:

   */README
   CHANGELOG
